package ArraysAndStrings;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class stringPermutationTest {

	String string1;
	String string2;
	boolean expectedOutput;
	
	@Parameters
	public static Collection<Object[]> data(){
		return Arrays.asList(new Object[][]{
				{"123", "123", true},
				{"123", "132", true},
				{"123", "321", true},
				{"132", "123", true},
				{"132", "132", true},
				{"123", "321", true},
				{"123", "456", false},
				{"356", "214", false},
				{"123", "851", false},
		});
	}
	
	public stringPermutationTest(String string1, String string2, boolean eOutput){
		this.string1 = string1;
		this.string2 = string2;
		this.expectedOutput = eOutput;
	}
}
