package ArraysAndStrings;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class uniqueCharactersTest {

	private String input;
	private boolean expectedOutput;
	
	public  uniqueCharactersTest(String input, boolean output){
		this.input = input;
		this.expectedOutput = output;
	}
	@Parameters
	public static Collection<Object[]> data(){
		return Arrays.asList( new Object[][]{
				{"aaa", false},
				{"aab", false},
				{"aba", false},
				{"baa", false},
				{"abc", true},
				{"bac", true},
				{"cab", true},
				{"This is a very long sentance that will show up false", false},
				{"abcdefghijklmnopqrstuvwxyz", true}
		});
	}
	@Test
	public void test() {
		assertEquals(expectedOutput, uniqueCharacters.uniqueChar(input));
	}

}
