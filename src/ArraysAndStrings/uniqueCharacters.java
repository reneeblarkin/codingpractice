package ArraysAndStrings;

import java.util.HashMap;

public class uniqueCharacters {
	
	public static boolean uniqueChar(String string){
		HashMap<String, String> characters = new HashMap<String, String>();
		for(int index = 0 ; index < string.length(); index++){
			if(characters.containsKey(""+string.charAt(index)+ "")){
				return false;
			}
			else{
				characters.put(""+string.charAt(index)+"", string.charAt(index)+"");
			}
		}
			
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(uniqueChar("This statement will print false."));
	}

}
